### 3.1.2  CNode方法

能力管理大部分通过调用CNode方法来实现。

CNodes支持以下方法：

- **seL4_CNode_Mint()** 在指定的CNode槽中从已有能力创建一个新能力。新创建的能力可能比原来的能力拥有更少的权限和不同的保护位(见3.3.1节)。seL4_CNode_Mint()也可以从未标记(unbadged)的能力创建一个有标记(badged)的能力(参见4.2.1节)。

- **seL4_CNode_Copy()** 类似于seL4_CNode_Mint()，但是新创建的能力与原始能力有相同的标记和保护位。

- **seL4_CNode_Move()** 在两个指定的能力slots之间移动能力。你
无法将能力移动到其当前位置。

- **seL4_CNode_Mutate()** 可以像seL4_CNode_Move()一样移动能力；还可以像seL4_CNode_Mint()那样减少它的权限，尽管没有保留原始副本。

- **seL4_CNode_Rotate()** 在三个指定的能力slots之间“旋转”移动两个能力。它实际上是两个seL4_CNode_Move()调用：一个由第二个slot移至第一个slot，另一个由第三个slot移至第二个slot。第一和第三个slot可能是相同的位置，在这种情况下就是交换其中的能力。方法是原子的：两个能力要么都移动了，要么都没有移动。

- **seL4_CNode_Delete()** 从指定的slot中删除一个能力。

- **seL4_CNode_Revoke()** 相当于对指定能力的每个派生子能力调用seL4_CNode_Delete()。它对指定的能力本身没有影响，除非是在第3.2节中描述的非常特殊的情况下。

- **seL4_CNode_SaveCaller()** 将内核为当前线程生成的回复能力，从创建时的特殊TCB槽移至指定的CSpace槽(仅非MCS配置)。

- **seL4_CNode_CancelBadgedSends()** 取消明确阻塞于指定(端点)对象的未完成发送，即，发送者确实是通过这个指定(端点)能力在发送，并且明确指示了与该能力相同的发送标记。
