除特别说明，以下时间均为译文时间。

2020-08-30 原文2.4.2节补上了SC可变大小的描述，删除了6.1.11.2中无用的最后一段。

2020-07-21  原文更新了ARM有关API描述，增加了AckVPPI方法；增加了三个基准测试系统调用：FlushL1Caches（目前仅支持ARM平台）、DumpAllThreadsUtilisation和ResetAllThreadsUtilisation。

2020-04-07  原文更新了个别文字，主要在第2章。

以原文2019-11-20 ver11.0.0为主要参照形成译文。
