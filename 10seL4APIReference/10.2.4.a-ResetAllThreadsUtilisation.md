#### 10.2.4.10  ResetAllThreadsUtilisation

LIBSEL4_INLINE_FUNC void seL4_BenchmarkResetAllThreadsUtilisation

重置当前CPU节点每个线程的累计周期计数。

类型 | 名字 | 描述
--- | --- | ---
void |  | 

*返回值*：无。

*描述*：将每个线程的周期计数重置为0。
